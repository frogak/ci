# Master Front End XI - Módulo 7 - Cloud - Laboratorio

### Básico

#### Ejercicio 1. Despliegue manual
Desplegar en Github Pages de forma manual:
* Tenemos un repo en Github.
* Queremos desplegar una página de demo.
* Realizar el despliegue manual.

#### Ejercicio 2. Despliegue automático
Automatizar el proceso de despliegue:
* Queremos que cada vez que se haga un merge a master se dispare un flujo de build y despliegue.
* Usar Github Actions para esto.


#### Solución
En vez de ofrecer la solucion en __GitHub__, la ofrezco en __GitLab__.

En __GitLab__ definimos los procesos __CI/CD__ en el archivo `.gitlab-ci.yml`

En este caso podemos presentar la solución a ambos ejercicios en el mismo proceso de __CI/CD__.

Ambos comparten `stages` y `jobs`
```
stages:
  - build
  - deploy

build_job:
  stage: build
  image: node:latest
  script:
    - npm install
    - npm run build
  artifacts:
    paths:
    - dist
  rules:
    vvvvvvvvvvvvvvvvvvvvvvv
    AQUI VAN LAS SOLUCIONES
    ^^^^^^^^^^^^^^^^^^^^^^^

pages:
  stage: deploy
  script:
    - mv dist/* public
  dependencies:
  - build_job
  artifacts:
    paths:
      - public
```

La solución en a ambos la enocntramos en la etiqueta `rules` del `build_job`

Para dar solución al __ejercicio 1__ (despliqgue manual) añadimos la siguiente regla, la cual define que si se crea la pipeline desde la web (manulamente), creará la pipeline, pero quedará a la espera de que el usuario la lance, también manualmente.
```
  - if: $CI_COMMIT_BRANCH == 'main' && $CI_PIPELINE_SOURCE == 'web'
    when: manual
```


Para dar solución al __ejercicio 2__ (despliqgue automático) añadimos la siguiente regla, la cual define que la pipeline se creará y lanzará  automáticamente, cuando se haga un `push` a la rama `main`.
```
  - if: $CI_COMMIT_BRANCH == 'main' && $CI_PIPELINE_SOURCE == 'push'
```


__NOTA:__ Como ha utilizado el template oficial para Vite que ofrece [Svelte en su web](https://svelte.dev/)
```
npm create vite@latest my-app -- --template svelte
```
Se han realizado unas poqueñas modificaciones para poder adaptarlo al despligue en GitLab Pages, cambiando las rutas absolutas por relativas.
Concretamente en los commits: [ee019882](https://gitlab.com/frogak/ci/-/commit/ee0198823225a30d0c56717e949869432730ac84) y [ee019882](https://gitlab.com/frogak/ci/-/commit/e39bcdc65d1926a1164ea401fd81deb611ffc7d6)
