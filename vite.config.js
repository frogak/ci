import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'


console.log(svelte()[1].transformIndexHtml().tags[0].attrs)

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte()],
  base: './'
})
